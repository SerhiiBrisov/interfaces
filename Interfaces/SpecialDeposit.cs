﻿using System;

namespace Interfaces
{
    public class SpecialDeposit : Deposit, IProlongable
    {
        public SpecialDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
        {

        }

        public bool CanToProlong()
        {
            if (Amount > 1000)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override decimal Income()
        {
            decimal income = Amount;
            for (int i = 1; i <= Period; i++)
            {
                income += income * i / 100;
            }
            return decimal.Round((income - Amount), 2, MidpointRounding.AwayFromZero);

        }
    }
}
