﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Interfaces
{
    public class Client : IEnumerable<Deposit>
    {
        private readonly Deposit[] deposits;

        public Client()
        {
            deposits = new Deposit[10];
        }

        public bool AddDeposit(Deposit deposit)
        {
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] is null)
                {
                    deposits[i] = deposit;
                    return true;
                }
            }
            return false;
        }

        public decimal TotalIncome()
        {
            decimal totalIncome = 0;
            foreach (var deposit in deposits)
            {
                if (!(deposit is null))
                {
                    totalIncome += deposit.Income();
                }
            }
            return totalIncome;
        }

        public decimal MaxIncome()
        {
            decimal maxIncome = 0;
            for (int i = 0; i < deposits.Length; i++)
            {
                if (!(deposits[i] is null) && deposits[i].Income() >= maxIncome)
                {
                    maxIncome = deposits[i].Income();
                }
            }

            return maxIncome;
        }

        public decimal GetIncomeByNumber(int number)
        {
            if ((deposits[number - 1] is null))
            {
                return 0;
            }
            return deposits[number - 1].Income();
        }

        public void SortDeposits()
        {
            Array.Sort(deposits);
            Array.Reverse(deposits);
        }

        public int CountPossibleToProlongDeposit()
        {
            int result = 0;
            foreach (var deposit in deposits)
            {
                if ((deposit is IProlongable))
                {
                    var depo = deposit as IProlongable;
                    if (depo.CanToProlong())
                    {
                        result++;
                    }
                }
            }
            return result;
        }

        public IEnumerator<Deposit> GetEnumerator()
        {
            return new DepositEnumerator(deposits);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Deposit>)deposits).GetEnumerator();
        }
    }

    internal class DepositEnumerator : IEnumerator<Deposit>
    {
        private Deposit[] deposits;
        private int position = -1;
        public DepositEnumerator(Deposit[] deposits)
        {
            this.deposits = deposits;
        }

        public Deposit Current
        {
            get
            {
                if (position == -1 || position >= deposits.Length)
                {
                    throw new InvalidOperationException();
                }

                return deposits[position];
            }
        }

        private object Current1
        {
            get { return this.Current; }
        }

        object IEnumerator.Current
        {
            get { return Current1; }
        }


        public bool MoveNext()
        {
            if (position < deposits.Length - 1)
            {
                position++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            position = -1;
        }

        // Implement IDisposable, which is also implemented by IEnumerator(T).
        private bool disposedValue = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue && disposing)
            {
                position = -1;
                deposits = null;
            }

            disposedValue = true;
        }

        ~DepositEnumerator()
        {
            Dispose(false);
        }
    }

}
