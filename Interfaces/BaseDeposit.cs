﻿using System;

namespace Interfaces
{
    public class BaseDeposit : Deposit
    {
        private const int baseRate = 5;
        public BaseDeposit(decimal depositAmount, int depositPeriod) : base(depositAmount, depositPeriod)
        {

        }
        public override decimal Income()
        {
            decimal income = Amount;
            for (int i = 0; i < this.Period; i++)
            {
                income += income * baseRate / 100;
            }
            return decimal.Round((income - Amount), 2, MidpointRounding.AwayFromZero);
        }
    }
}