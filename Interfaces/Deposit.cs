﻿using System;

namespace Interfaces
{
    public abstract class Deposit : IComparable<Deposit>
    {
        public decimal Amount { get; }
        public int Period { get; }

        protected Deposit(decimal depositAmount, int depositPeriod)
        {
            Amount = depositAmount;
            Period = depositPeriod;
        }

        public abstract decimal Income();

        public int CompareTo(Deposit other)
        {
            // If other is not a valid object reference, this instance is greater.
            if (other is null)
            {
                return 1;
            }
            else
            {
                return (Amount + Income()).CompareTo(other.Amount + other.Income());
            }
        }

        // Define the is greater than operator.
        public static bool operator >(decimal operand1, Deposit operand2)
        {
            return operand1.CompareTo(operand2.Amount + operand2.Income()) == 1;
        }

        // Define the is less than operator.
        public static bool operator <(decimal operand1, Deposit operand2)
        {
            return operand1.CompareTo(operand2.Amount + operand2.Income()) == -1;
        }

        // Define the is greater than or equal to operator.
        public static bool operator >=(decimal operand1, Deposit operand2)
        {
            return operand1.CompareTo(operand2.Amount + operand2.Income()) >= 0;
        }

        // Define the is less than or equal to operator.
        public static bool operator <=(decimal operand1, Deposit operand2)
        {
            return operand1.CompareTo(operand2.Amount + operand2.Income()) <= 0;
        }

        public static bool operator ==(Deposit operand1, Deposit operand2)
        {
            if (operand1 is null)
            {
                return false;
            }
            else
            {
                return operand1.CompareTo(operand2) <= 0;
            }
        }

        public static bool operator !=(Deposit operand1, Deposit operand2)
        {
            if (operand1 is null)
            {
                return true;
            }
            else
            {
                return operand1.CompareTo(operand2) >= 0;
            }
        }

        public override bool Equals(object obj)
        {
            return (CompareTo(obj as Deposit) == 0);
        }

        public override int GetHashCode()
        {
            return Amount.GetHashCode() + Period.GetHashCode();
        }

    }
}
